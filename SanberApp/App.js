// App.js untuk Tugas12
// Tugas12 untuk menampilkan youtubeUI
//import { StatusBar } from 'expo-status-bar';
//import React from 'react';
// import { StyleSheet, Text, View } from 'react-native';

// import YoutubeUI from './Tugas/Tugas12/App';

// export default function App() {
//   return (
//       <YoutubeUI />
//   );
// }

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     backgroundColor: '#fff',
//     alignItems: 'center',
//     justifyContent: 'center',
//   },
// });

// ********************************************************
// App.js untuk Tugas13
// Tugas13 untuk menampilkan Styling & Flexbox
//import { StatusBar } from 'expo-status-bar';
//import React from 'react';
// import { StyleSheet, Text, View } from 'react-native';

// import LoginScreen from './Tugas/Tugas13/LoginScreen'
// import AboutScreen from './Tugas/Tugas13/AboutScreen'

// export default function App() {
//   return (
//       <LoginScreen/>
//       <AboutScreen/>
//   );
// }

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     backgroundColor: '#fff',
//     alignItems: 'center',
//     justifyContent: 'center',
//   },
// });

// ************************************************************
// App.js Tugas14
// Tugas 14 Component API & Lifecycle
// import React from 'react';
// import Main from './Tugas/Tugas14/components/Main';
// import SkillScreen from './Tugas/Tugas14/SkillScreen';
// export default class App extends React.Component {
//   render() {
//     return (
//        // soal No 1
// //      <Main />
//       // Soal No 2
//       <SkillScreen/>
//     )
//   }
// }

// ************************************************************
// App.js Tugas15
// Tugas 15 React Navigation
// import { NavigationContainer } from "@react-navigation/native";
// import { createStackNavigator } from '@react-navigation/stack';
// import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

// import {SignIn, CreateAccount, Profile, Home} from './Screen';

// const AuthStack = createStackNavigator();
// const Tabs = createBottomTabNavigator();
// const HomeStack = createStackNavigator();
// const ProfileStack = createStackNavigator();

// const HomeStackScreen = () => (
//   <HomeStack.Navigator>
//     <HomeStack.Screen name="Home" component={Home}/>
//   </HomeStack.Navigator>
// )

// const ProfileStackScreen = () => (
//   <ProfileStack.Navigator>
//     <ProfileStack.Screen name="Profile" component={Profile}/>
//   </ProfileStack.Navigator>
// )

// export default () => (
//   <NavigationContainer>
//     <Tabs.Navigator>
//       <Tabs.Screen name="Home" component={HomeStackScreen} />
//       <Tabs.Screen name="Profile" component={ProfileStackScreen} />
//     </Tabs.Navigator>
//     {/* <AuthStack.Navigator>
//       <AuthStack.Screen name="SignIn" component={SignIn} options={{title: 'Sign In'}} />
//       <AuthStack.Screen name="CreateAccount" component={CreateAccount} options={{title: 'Create Account'}} />
//     </AuthStack.Navigator> */}

//   </NavigationContainer>
// );

//********************************************** */
// Quiz 3
//*********************************************** */
import * as React from 'react';
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import 'react-native-gesture-handler';

import LoginScreen from './Tugas/Quiz3/LoginScreen';
import HomeScreen from './Tugas/Quiz3/HomeScreen';
import Index from './Tugas/Quiz3/index';


export default class App extends React.Component {
    render() {
      return (
         
        <HomeScreen/>
//         <LoginScreen/>
      )
    }
  }

// const Stack = createStackNavigator();

// export default class App extends React.Component {
//   render() {
//     return (
//       <NavigationContainer>
//         <Stack.Navigator initialRouteName="Home" >
//           <Stack.Screen name='Login' component={LoginScreen} options={{ headerShown: false }} />
//           <Stack.Screen name='Home' component={HomeScreen} options={{ headerTitle: 'Daftar Barang' }} />
//         </Stack.Navigator>
//       </NavigationContainer>
//     );
//   }
// }


