import React from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';


export default class SkillItem extends React.Component {
    render() {
       // alert(data.kind);
        let skill = this.props.skill;
        return (
            <View style={styles.container}>
                <View style={styles.descContainer}>
                    
                    <View style={styles.skillDetails}>
                        <Text style={styles.skillTitle} >{skill.skillName} </Text>
                        <Text style={styles.skillStats} >{skill.categoryName + " . " + nFormatter(skill.statistics.viewCount,1)} </Text>
                    </View>
                    
                </View>

            </View>
        )
    }

}

function nFormatter(num, digits) {
    var si = [
      { value: 1, symbol: "" },
      { value: 1E3, symbol: "k" },
      { value: 1E6, symbol: "M" },
      { value: 1E9, symbol: "G" },
      { value: 1E12, symbol: "T" },
      { value: 1E15, symbol: "P" },
      { value: 1E18, symbol: "E" }
    ];
    var rx = /\.0+$|(\.[0-9]*[1-9])0+$/;
    var i;
    for (i = si.length - 1; i > 0; i--) {
      if (num >= si[i].value) {
        break;
      }
    }
    return (num / si[i].value).toFixed(digits).replace(rx, "$1") + si[i].symbol + 'views';
  }

const styles = StyleSheet.create({
    container: {
        padding: 15
    },
    descContainer: {
        flexDirection: 'row',
        paddingTop: 15
    },
    skillTitle: {
        fontSize: 15,
        color: '#3c3c3c'
    },
    skillDetails: {
        paddingHorizontal: 15,
        flex: 1
    },
    skillStats: {
        fontSize: 12,
        paddingTop: 3
    }
});