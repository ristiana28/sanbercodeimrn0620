import React from 'react';
import { StyleSheet, Text, View, Image, FlatList } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import data from './skillData.json';
//import SkillItem from './SkillItem';


export default class SkillScreen extends React.Component {
  
  render(){
    //alert(data.kind);
    return (
        <View style={styles.container}>
          <View style={styles.containerBox}>
            <Image style={styles.logo}
                source={require('./images/sanber.png')} >
            </Image>
            <View style={styles.containerCircle}>
                <Icon style={styles.navItem} name="account-circle" size={32}/>
                  <View style={styles.containerName}>
                    <Text style={styles.containerText}>Rina Ristiana</Text>
                    <Text style={styles.containerHai} >Hai</Text>
                  </View>
            </View>
            <View style={styles.containerSkill} >
              <Text style={styles.SkillDo}>SKILL</Text>
            </View>
            <View style={styles.containerLine} >
            </View>
          </View>
          <View style={styles.ItemName} >
            <Text style={styles.TitleLib} > Library/Framework</Text>
            <Text style={styles.TitleProg} > Bahasa Pemrograman</Text>
            <Text style={styles.TitleTech} > Teknologi</Text>
          </View>
          <View style={styles.body}>
            <Image style={styles.iconReact}
                source={require('./images/reactIcon.png')} >
            </Image>
            <Icon style={styles.iconItem} name="keyboard-arrow-right" size={100}/>
            <Text style={styles.textTitle} >React Native</Text>
            <Text style={styles.textTitleLib} >Library/Framework</Text>
            <Text style={styles.textTitlePercen} >50%</Text>
          </View>
          {/* <View style={styles.body}>
            <Image style={styles.iconReact}
                source={require('./images/reactIcon.png')} >
            </Image>
            <Icon style={styles.iconItem} name="keyboard-arrow-right" size={100}/>
            <Text style={styles.textTitle} >React Native</Text>
            <Text style={styles.textTitleLib} >Library/Framework</Text>
            <Text style={styles.textTitlePercen} >50%</Text>
          </View>
          <View style={styles.body1}>
            <Image style={styles.iconLavarel}
                source={require('./images/reactIcon.png')} >
            </Image>
            <Icon style={styles.iconItem} name="keyboard-arrow-right" size={100}/>
            <Text style={styles.textTitle} >Lavarel</Text>
            <Text style={styles.textTitleLib} >Library/Framework</Text>
            <Text style={styles.textTitlePercen} >100%</Text>
          </View>
          <View style={styles.body2}>
            <Image style={styles.iconReact}
                source={require('./images/reactIcon.png')} >
            </Image>
            <Icon style={styles.iconItem} name="keyboard-arrow-right" size={100}/>
            <Text style={styles.textTitle} >React Native</Text>
            <Text style={styles.textTitleLib} >Library/Framework</Text>
            <Text style={styles.textTitlePercen} >50%</Text>
          </View>
          <View style={styles.body3}>
            <Image style={styles.iconReact}
                source={require('./images/reactIcon.png')} >
            </Image>
            <Icon style={styles.iconItem} name="keyboard-arrow-right" size={100}/>
            <Text style={styles.textTitle} >React Native</Text>
            <Text style={styles.textTitleLib} >Library/Framework</Text>
            <Text style={styles.textTitlePercen} >50%</Text>
          </View> */}


        </View>
        
    );
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#E5E5E5',
  },
  containerBox: {
    position: 'absolute',
    width: 375,
    height: 155,
    left: 0,
    top: 0,
    backgroundColor: '#FFFFFF'
  },
  logo: {
    position: 'absolute',
    width: 187,
    height: 54,
    left: 170,
    top: 10
  },
  containerCircle: {
    position: 'absolute',
    width: 147,
    height: 33,
    left: 16,
    top: 54,
    backgroundColor: '#E5E5E5'
  },
  navItem: {
    position: 'absolute',
    left: 0,
    backgroundColor: '#3EC6FF'
  },
  containerName: {
      position: 'absolute',
      left: 30,
      width: 147,
      height: 33,
      top: 0,
      backgroundColor: 'white',
  },
  containerText: {
    position: 'absolute',
    width: 107,
    height: 19,
    left: 5,
    top: 15,
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontSize: 16,
    lineHeight: 19,
    color: '#003366'
  },
  containerHai: {
    position: 'absolute',
    width: 107,
    height: 19,
    left: 5,
    top: 0,
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontWeight: 'normal',
    fontSize: 12,
    lineHeight: 14,
    color: '#000000'
  },
  containerSkill: {
    position: 'absolute',
    left: 16,
    width: 147,
    height: 33,
    top: 98,
    backgroundColor: '#FFFFFF'
  },
  SkillDo: {
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontWeight: 'normal',
    fontSize: 36,
    lineHeight: 42,
    color: '#003366'
  },
  containerLine: {
    position: 'absolute',
    left: 16,
    width: 335,
    height: 4,
    top: 145,
    backgroundColor: '#3EC6FF',
  },
  ItemName: {
    position: 'absolute',
    width: 375,
    height: 40,
    left: 0,
    top: 156,
    backgroundColor: '#FFFFFF',
    flexDirection: 'row',
    flex: 1
  },
  TitleTech: {
    width: 70,
    height: 30,
    left: 25,
    top: 5,
    backgroundColor: '#84E9FF',
    borderRadius: 8,
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: 12,
    lineHeight: 14,
    color: '#003366',
    padding: 6,
    alignItems: 'center'
  },
  TitleProg: {
    width: 130,
    height: 30,
    left: 18,
    top: 5,
    backgroundColor: '#84E9FF',
    borderRadius: 8,
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: 11,
    lineHeight: 14,
    color: '#003366',
    padding: 8,
    alignItems: 'center'
  },
  TitleLib: {
    width: 125,
    height: 30,
    left: 10,
    top: 5,
    backgroundColor: '#84E9FF',
    borderRadius: 8,
    fontFamily: 'Roboto',
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: 12,
    lineHeight: 14,
    color: '#003366',
    padding: 8,
    alignItems: 'center'
  },
  body: {
    position: 'absolute',
    width: 335,
    height: 129,
    left: 14,
    top: 201,
    backgroundColor: '#84E9FF',
    borderWidth: 1,
    borderColor: 'rgb(0, 0, 0.25)',
    shadowColor: 'red',
    shadowRadius: 25,
    shadowOpacity: 2,
    flex: 1,
    borderRadius: 8
  },
  textTitle: {
      fontWeight: 'bold',
      fontSize: 24,
      color: '#003366',
      left: 110,
      top: 5,
      lineHeight: 28
  },
  textTitleLib: {
    fontWeight: 'bold',
    fontSize: 16,
    color: '#3EC6FF',
    left: 110,
    top: 5,
    lineHeight: 19
},
textTitlePercen: {
  fontWeight: 'bold',
  fontSize: 48,
  color: '#FFFFFF',
  left: 158,
  top: 10,
  lineHeight: 56
},
iconReact: {
  position: 'absolute',
  width: 100,
  height: 100,
  left: 4,
  top: 13,
  backgroundColor: '#84E9FF'
},
iconItem: {
  position: 'absolute',
  width: 70,
  height: 100,
  left: 260,
  top: 13,
  backgroundColor: '#84E9FF'
},
body1: {
  position: 'absolute',
  width: 335,
  height: 129,
  left: 14,
  top: 150,
  backgroundColor: '#84E9FF',
  borderWidth: 1,
  borderColor: 'rgb(0, 0, 0.25)',
  shadowColor: 'red',
  shadowRadius: 25,
  shadowOpacity: 2,
  flex: 1,
  borderRadius: 8
},






  descContainer: {
    flexDirection: 'row',
    paddingTop: 75,
    padding: 10
  },
  title1: {
    fontSize: 16,
    color: 'white'
  },
  navBar: {
    height: 60,
    backgroundColor: 'white',
    elevation: 3,
    paddingHorizontal: 100,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  title2: {
    fontSize: 16,
    color: 'white',
    paddingTop: 65
  },
  title3: {
    fontSize: 10,
    color: 'white',
    justifyContent: 'flex-end'
  },
  navBar2: {
    height: 30,
    elevation: 3,
    paddingHorizontal: 100,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },

});