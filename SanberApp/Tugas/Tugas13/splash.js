import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

export default class Splash extends React.Component {
  constructor(props) {
    super(props)
    this.state={timer: 0}
    setInterval(()=>{
      this.setState({timer: this.state.timer + 1})
    }, 3000)

  }
  render(){
    return (
      <View style={styles.container}>
        <Text style={styles.title}>{`Welcome to SanberApp: ${this.state.timer}`}</Text>
        <Text style={styles.title}>{`Hello Sanber`}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#003f5c',
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
      fontWeight: 'bold',
      fontSize: 18,
      color: 'white'
  }
});