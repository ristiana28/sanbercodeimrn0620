import React from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';

export default class AboutScreen extends React.Component {
  render(){
    return (
      <View style={styles.container}>
        <Text style={styles.title}>Welcome to SanberApp</Text>
        <Text style={styles.title}>Hello Sanber</Text>
          <View style={styles.descContainer}>
            <Image source={require('./images/rina.png')} style={{width:50, height:50, borderRadius: 25}} />
          </View>
          <Text style={styles.title} >About Me</Text>
          <Text style={styles.title1} >Hai, I am Rina Ristiana</Text>
          <Text style={styles.title1} >I am a beginner proggrammer </Text>
          <Text style={styles.title1} >my skill programming is: </Text>
            <View style={styles.container}>
                <View style={styles.navBar}>
                    <Image source={require('./images/javascript.jpeg')} style={{width:50, height: 50}} />
                    <Image source={require('./images/androstu.jpeg')} style={{width:90, height: 50}} />
                    <Image source={require('./images/react.jpeg')} style={{width:50, height: 50}} />
                    <Image source={require('./images/python.jpeg')} style={{width:50, height: 50}} />
                </View>
                <Text style={styles.title2} > call me </Text>
            </View>
            <View style={styles.navBar2}>
                <Image source={require('./images/whatsapp.png')} style={{width:20, height: 20}}/>
                    <Text style={styles.title3} > 081379917559 </Text>
                <Image source={require('./images/facebook.jpeg')} style={{width:20, height: 20}}/>
                    <Text style={styles.title3} > Rina Ristiana </Text>
                <Image source={require('./images/gitlab.png')} style={{width:20, height: 20}}/>
                     <Text style={styles.title3} > ristiana28 </Text>
            </View>
      </View>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#003f5c',
    alignItems: 'center',
    padding: 40,
    flexDirection: 'column'
  },
  title: {
      fontWeight: 'bold',
      fontSize: 18,
      color: 'white',
  },
  descContainer: {
    flexDirection: 'row',
    paddingTop: 75,
    padding: 10
  },
  title1: {
    fontSize: 16,
    color: 'white'
  },
  navBar: {
    height: 60,
    backgroundColor: 'white',
    elevation: 3,
    paddingHorizontal: 100,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  title2: {
    fontSize: 16,
    color: 'white',
    paddingTop: 65
  },
  title3: {
    fontSize: 10,
    color: 'white',
    justifyContent: 'flex-end'
  },
  navBar2: {
    height: 30,
    elevation: 3,
    paddingHorizontal: 100,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },

});