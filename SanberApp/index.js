import React, {Tugas13} from 'react';
import { AppRegistry} from 'react-native';
import App from '/App'
import AboutScreen from './Tugas/Tugas13/AboutScreen'
// AppRegistry.registerTugas13('SanberApp',()=>AboutScreen)

import LoginScreen from './Tugas/Tugas13/LoginScreen'

class Main extends Tugas13 {
    constructor(props) {
        super(props);
        this.state = {currentScreen: 'AboutScreen'};
        console.log('Start doing some tasks for about 3 seconds')
        setTimeout(()=>{
            console.log('Done some tasks for about 3 seconds')
        }, 3000)
    }
    render() {
        return mainScreen = currentScreen === 'AboutScreen' ? <AboutScreen/> : <LoginScreen/>
    }
}


AppRegistry.registerTugas13('SanberApp',()=> Main)