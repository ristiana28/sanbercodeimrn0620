// soal 1 Range

var start = [ ];
var finish = [ ]

function range(start, finish) {
    if(start < finish) {
        for(var start; start < finish + 1; start++) {
            console.log(start);
        }
    } else if(start > finish) {
        for(var start; start > finish - 1; start--) {
            console.log(start);
        }    
    }else if(start == " " && finish == " ") {
        console.log("-1")
    }
}
console.log("Soal no 1 Range ================")
console.log(" Range(1,10) // Output: [1,2,3,4,5,6,7,8,9,10]");
range(1, 10);
console.log("range( , ) // Output: [-1]");
range(" "," ");
console.log("range(11, 18) // Output: [11,12,13,14,15,16,17,18]");
range(11,18);
console.log("range(54,50) // Output: [54,53,52,51,50]");
range(54, 50);
console.log("range( , ) // Output: [-1]");
range(" "," ");
console.log(" "); // jarak saja
console.log(" "); // jarak saja


// Soal No 2 Range with Step
var awal = [ ]
var akhir = [ ]
var step = [ ]

function rangeWithStep(awal, akhir, step) {
    if(awal < akhir) {
        for(var awal; awal < akhir + 1; awal+= step) {
            console.log(awal);
        }
    } else if(awal > akhir) {
        for(var awal; awal > akhir - 1; awal-=step) {
            console.log(awal);
        }    
    }else if(awal == " " && akhir == " ") {
        console.log("-1")
    }
}
console.log("Soal no 2 Range with Step ================")
console.log(" RangeWithStep(1,10, 2) // Output: [1,3,5,7,9]");
rangeWithStep(1, 10, 2);
console.log("rangeWithStep(11, 23, 3) // Output: [11,14,17,20,23]");
rangeWithStep(11, 23, 3);
console.log("rangeWithStep(5, 2, 1) // Output: [5,4,3,2]");
rangeWithStep(5, 2, 1);
console.log("rangeWithStep(29, 2, 4) // Output: [29,25,21,17,13,9,5]");
rangeWithStep(29, 2, 4);
console.log(" "); // jarak saja
console.log(" "); // jarak saja

// Soal No 3 Sum of Range
var derAwal = [ ]
var derAkhir = [ ]
var derStep = [ ]
var jumlah = 0

function sum(derAwal, derAkhir) {
    if(derAwal < derAkhir) {
       for (var i = derAwal; i <= derAkhir; i++) {
           jumlah = jumlah + i;
       }
       console.log(jumlah);
   }
   return jumlah;
}

console.log("Soal No 3 Sum of Range ================");
console.log("sum(1,10) // Output: 55");
sum(1, 10);


var derAwal1 = [ ]
var derAkhir2 = [ ]
var derStep2 = [ ]
var jumlah2 = 0
function sum2(derAwal2, derAkhir2,derStep2) {
    if(derAwal2 < derAkhir2) {
       for (var i2 = derAwal2; i2 <= derAkhir2; i2+=derStep2) {
           jumlah2 = jumlah2 + i2;
       }
       console.log(jumlah2);
   }
   return jumlah2;
}

console.log("sum(5, 50, 2) // Output: 621");
sum2(5, 50, 2);

var derAwal3 = [ ]
var derAkhir3 = [ ]
var derStep3 = [ ]
var jumlah3 = 0
function sum3(derAwal3, derAkhir3) {
    if (derAwal3 > derAkhir3) {
        for (var i3 = derAwal3; i3 >= derAkhir3; i3--) {
            jumlah3 = jumlah3 + i3;
        }
        console.log(jumlah3);
    }
    return jumlah3;
}
console.log("sum(15, 10) // Output: 75");
sum3(15, 10);

var derAwal4 = [ ]
var derAkhir4 = [ ]
var derStep4 = [ ]
var jumlah4 = 0
function sum4(derAwal4, derAkhir4,derStep4) {
    if(derAwal4 > derAkhir4) {
       for (var i4 = derAwal4; i4 >= derAkhir4; i4-=derStep4) {
           jumlah4 = jumlah4 + i4;
       }
       console.log(jumlah4);
   }
   return jumlah4;
}

console.log("sum(20, 10, 2) // Output: 90");
sum4(20, 10, 2);

var n = []
var jumlah5 = 0
function sum5(n) {
    for (var i5 = 0; i5 <= n; i5++) {
        jumlah5 = jumlah5 + i5;
    }
    console.log(jumlah5);
    return jumlah5;
}

console.log("sum(0) // Output: 0");
sum5(0);
console.log("sum(1) // Output: 1")
sum5(1);
console.log(" "); // jarak saja
console.log(" "); // jarak saja


// Soal No 4 Array Multidimensi
//contoh input
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
];

function dataHandLing() {
    console.log("Soal 4 Array Multidimensi ===================")

    console.log("Nomor ID: " + input[0][0]);
    console.log("Nama Lengkap: " + input[0][1]);
    console.log("TTL: " + input[0][2] + input[0][3]);
    console.log("Hobi: " + input[0][4]);
    console.log("");

    console.log("Nomor ID: " + input[1][0]);
    console.log("Nama Lengkap: " + input[1][1]);
    console.log("TTL: " + input[1][2] + input[1][3]);
    console.log("Hobi: " + input[1][4]);
    console.log("");

    console.log("Nomor ID: " + input[2][0]);
    console.log("Nama Lengkap: " + input[2][1]);
    console.log("TTL: " + input[2][2] + input[2][3]);
    console.log("Hobi: " + input[2][4]);
    console.log("");

    console.log("Nomor ID: " + input[3][0]);
    console.log("Nama Lengkap: " + input[3][1]);
    console.log("TTL: " + input[3][2] + input[3][3]);
    console.log("Hobi: " + input[3][4]);
    console.log("");
    
    return input;
}

dataHandLing();
console.log(" "); // jarak saja
console.log(" "); // jarak saja

// Soal 5 Balik Kata
var word = String;
var newKata = " ";

function balikKata(word) {
    for(var i = word.length -1 ; i >= 0; i--) {
        newKata = newKata + word[i];
    }
    console.log(newKata);
    return newKata;
}
console.log("Soal 5 Baik Kata ===================")
balikKata("  Kasur Rusak");
balikKata("  SanberCode");
balikKata("  Haji Ijah");
balikKata("  racecar");
balikKata("  I am Sanbers");
console.log(" "); // jarak saja
console.log("komentar sedikit, kenapa fungsi ini tidak bisa dipakai berulang")
console.log("kalo dipakai berulang seolah ditambahkan....heran")
console.log(" "); // jarak saja
console.log(" "); // jarak saja

// Soal no 6 Metoda Array
var data1 = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"];
function dataHandLing2(data1) {
    console.log("Soal 6 Metoda Array ================")
    console.log(data1);
    data1.splice(1, 1, "Roman Alamsyah Elsharawy");
    console.log(data1);
    var data2 = data1[3].split("/");
    console.log(data2);
    switch (data2[1]) {
        case "05":
            teks = 'Mei';
            break;
    }
    console.log(teks);
    var data3 = data2.join("-")
    console.log(data3);
    var data4 = data1[1].slice(0,15);
    console.log(data4);
} 
dataHandLing2(data1);