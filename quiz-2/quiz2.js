/**
 * Berikut soal quiz kali ini, terdiri dari 3 Soal
 * Kerjakan dengan sebaik mungkin, dengan menggunakan metode yang telah dipelajari,
 * Tidak diperkenankan untuk menjawab hanya dengan console.log('teks jawaban');
 * maupun dengan terlebih dahulu memasukkannya ke dalam variabel, misal var a = 'teks jawaban'; console.log(a);
 * 
 * Terdapat tambahan poin pada setiap soal yang dikerjakan menggunakan sintaks ES6 (+5 poin)
 * Jika total nilai Anda melebihi 100 (nilai pilihan ganda + coding), tetap akan memiliki nilai akhir yaitu 100
 * 
 * Selamat mengerjakan
*/

/*========================================== 
  1. SOAL CLASS SCORE (10 poin + 5 Poin ES6)
  ==========================================
  Buatlah sebuah class dengan nama Score. class Score tersebut memiliki properti "subject", "points", dan "email". 
  "points" dapat di input berupa number (1 nilai) atau array of number (banyak nilai).
  tambahkan method average untuk menghitung rata-rata dari parameter points ketika yang di input berupa array (lebih dari 1 nilai)
*/

class Score {
    // Code disini
    constructor(subject) {
        this.subject = subject;
        this.points = [40,54, 25, 46, 37, 55, 57, 66, 67, 70, 80];
        this.email = "ristiana28@gmail.com";
        var total = 0;
        var byknilai = this.points.length;
        for (var i = 0; i<this.points.length; i++) {
            total += [i];
        }
    }
    get average() {
        return console.log(total/this.points.length)
    }
  }
    console.log("Soal no 1")
    var angka = new Score("matematic");
    console.log(angka.subject); // matematic
    console.log(angka.points); // [40,54, 25, 46, 37, 55, 57, 66, 67, 70, 80]
    console.log(angka.email); // ristiana28@gmail.com
    
  
  /*=========================================== 
    2. SOAL Create Score (10 Poin + 5 Poin ES6)
    ===========================================
    Membuat function viewScores yang menerima parameter data berupa array multidimensi dan subject berupa string
    Function viewScores mengolah data email dan nilai skor pada parameter array 
    lalu mengembalikan data array yang berisi object yang dibuat dari class Score.
    Contoh: 
  
    Input
     
    data : 
    [
      ["email", "quiz-1", "quiz-2", "quiz-3"],
      ["abduh@mail.com", 78, 89, 90],
      ["khairun@mail.com", 95, 85, 88]
    ]
    subject: "quiz-1"
  
    Output 
    [
      {email: "abduh@mail.com", subject: "quiz-1", points: 78},
      {email: "khairun@mail.com", subject: "quiz-1", points: 95},
    ]
  */
  
  const data = [
    ["email", "quiz - 1", "quiz - 2", "quiz - 3"],
    ["abduh@mail.com", 78, 89, 90],
    ["khairun@mail.com", 95, 85, 88],
    ["bondra@mail.com", 70, 75, 78],
    ["regi@mail.com", 91, 89, 93]
  ]
  
  function viewScores(data, subject) {
    // code kamu di sini
        var arrdata = [ ];
        for(var i=0; i<data.length; i++) {
        
        var objdata = {
            "email " : data[i][0],
            "quiz-1 " : data[i][1],
            "quiz-2" : data[i][2],
            "quiz-3" : data[i][3],
            }
        arrdata.push(objdata);
        }
        var ObjNew = [];
        for(var j = 0; j<data.length; j++) {
        var newObj = [
            j+1+"." + data[j][0] + " " + data[j][1] + ":" ,
            arrdata[j] 
        ]
        ObjNew.push(newObj);
        }
        console.log(ObjNew);
}
  
  // TEST CASE
  console.log("soal no 2")
  viewScores(data, "quiz-1")
  viewScores(data, "quiz-2")
  viewScores(data, "quiz-3")

  
  
  /*==========================================
    3. SOAL Recap Score (15 Poin + 5 Poin ES6)
    ==========================================
      Buatlah fungsi recapScore yang menampilkan perolehan nilai semua student. 
      Data yang ditampilkan adalah email user, nilai rata-rata, dan predikat kelulusan. 
      predikat kelulusan ditentukan dari aturan berikut:
      nilai > 70 "participant"
      nilai > 80 "graduate"
      nilai > 90 "honour"
  
      output:
      1. Email: abduh@mail.com
      Rata-rata: 85.7
      Predikat: graduate
  
      2. Email: khairun@mail.com
      Rata-rata: 89.3
      Predikat: graduate
  
      3. Email: bondra@mail.com
      Rata-rata: 74.3
      Predikat: participant
  
      4. Email: regi@mail.com
      Rata-rata: 91
      Predikat: honour
  
  */

 const data2 = [
    ["email", "rata-rata", "predikat"],
    ["abduh@mail.com", 85.7,"graduate"],
    ["khairun@mail.com", 89.3,"graduate"],
    ["bondra@mail.com", 74.3,"perticipant"]
    ["regi@mail.com", 91,"honour"]
  ]

  function recapScores(data2) {
    // code kamu di sini
    var arrdata2 = [ ];
    for(var k=0; k<data2.length; k++) {

    var objdata2 = {
    "email " : data2[k][1],
    "rata-rata " : data2[k][2],
    "predikat" : data2[k][3],
    }
    arrdata2.push(objdata2);        
    }
    var ObjNew2= [];
    for(var m = 0; m<data.length; m++) {
    var newObj2 = [
    j+1+"." + data2[m][0] + " " + data2[m][1] + ":" ,
    arrdata[m] 
    ]
    ObjNew2.push(newObj2);
    }
    console.log(arrdata2);
    if(predicat >= 90) {
        console.log("honour");
    }else if(predicat >= 80) {
        console.log('graduate');
    } else if(predicat >= 70) {
        console.log("participant")
    }
}

// TEST CASE
console.log("soal no 3")
recapScores(data2, 90);
//