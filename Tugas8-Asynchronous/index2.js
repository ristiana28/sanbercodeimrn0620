var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise 
console.log("-----------------Soal 2 Promise ------------------")
var tampung = 10000;
function baca(x) {
    if(x == books.length) {
        return 0;
    }
    readBooksPromise(tampung,books[x])
    .then(function (fulfilled) {
        console.log(fulfilled);
    })
    .catch(function (error) {
        console.log(error.message);
    })
    
    baca(x+1);
}
baca(0);



// var tampung = 10000;
// function readBooks() {
//     readBooksPromise
//         .then(function(fulfilled) {
//             console.log(fulfilled);
//         })
//         .catch(function (error) {
//             console.log(error.message);
//         } );
// }
// readBooksPromise();
