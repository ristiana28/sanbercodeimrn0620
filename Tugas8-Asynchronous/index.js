// di index.js
var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Tulis code untuk memanggil function readBooks di sini
console.log("-----------------Soal 1 Callback------------------")
var tampung = 10000;
function baca(x) {
    if(x == books.length) {
        return 0;
    }
    readBooks(tampung,books[x],function(callback) {
        tampung = callback;
    } );
    baca(x+1);
}
baca(0);