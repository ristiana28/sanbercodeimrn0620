// Soal 1 mengubah function menjadi function arrow
console.log("Soal 1 Mengubah function menjadi function Arrow");
console.log("------------------------------------------------");
// Function ES5
//const golden = function goldenFunction(){
//    console.log("this is golden!!")
//  }
//  golden()

//Function Arrow ES6
const golden =  () => {
    console.log("this is golden");
}
golden(); //this is golden


// Soal 2 Object Literal di ES6
console.log("");
console.log("Soal 2 Object Literal di ES6");
console.log("------------------------------------------------");
// ES5
// const newFunction = function literal(firstName, lastName){
//     return {
//       firstName: firstName,
//       lastName: lastName,
//       fullName: function(){
//         console.log(firstName + " " + lastName)
//         return 
//       }
//     }
//   }
//   //Driver Code 
//   newFunction("William", "Imoh").fullName() 

//ES6
const newFunction = (firstName, lastName) => {
    console.log(firstName + " " + lastName);
    };
newFunction("Wiliam", "Imoh");


//Soal 3 Destructuring
console.log("");
console.log("Soal 3 Destructuring");
console.log("------------------------------------------------");
//ES5
// const newObject = {
//     firstName: "Harry",
//     lastName: "Potter Holt",
//     destination: "Hogwarts React Conf",
//     occupation: "Deve-wizard Avocado",
//     spell: "Vimulus Renderus!!!"
//   }
// const firstName = newObject.firstName;
// const lastName = newObject.lastName;
// const destination = newObject.destination;
// const occupation = newObject.occupation;
// Driver code
//console.log(firstName, lastName, destination, occupation)

//ES6
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
  }
const{firstName, lastName, destination, occupation} = newObject;
console.log(firstName, lastName, destination, occupation);


// Soal 4 Array Spreading
console.log("");
console.log("Soal 4 Array Spreading");
console.log("------------------------------------------------");
//ES5
// const west = ["Will", "Chris", "Sam", "Holly"]
// const east = ["Gill", "Brian", "Noel", "Maggie"]
// const combined = west.concat(east)
// //Driver Code
// console.log(combined)

//ES6
const west = ["Will", "Chris", "Sam", "Holly"];
const east = ["Gill", "Brian", "Noel", "Maggie"];
let combined = [...west, ...east];
console.log(combined);

// Soal 5 Template Literals
console.log("");
console.log("Soal 5 Template Literals");
console.log("------------------------------------------------");
//ES5
// const planet = "earth"
// const view = "glass"
// var before = 'Lorem ' + view + 'dolor sit amet, ' +  
//     'consectetur adipiscing elit,' + planet + 'do eiusmod tempor ' +
//     'incididunt ut labore et dolore magna aliqua. Ut enim' +
//     ' ad minim veniam'
// // Driver Code
// console.log(before) 

//ES6
const planet = "earth"
const view = "glass"
const before = `Lorem  ${view} dolor sit amet, 
consectetur adipiscing elit ${planet}
do eiusmod tempor incididunt ut labore et dolore magna aliqua.
Ut enim ad minim veniam`;
console.log(before);

