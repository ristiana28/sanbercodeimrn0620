// Soal 1 Animal Class
console.log("-------------- Soal 1 Animal Class - Release 0 -----------------");
console.log("-----------------menggunakan metoda getter----------------------");
// Buatlah class Animal yang menerima satu parameter constructor berupa name.
// Secara default class Animal akan memiliki property yaitu 
// legs (jumlah kaki) yang bernilai 4 dan cold_blooded bernilai false.

class Animal {
    constructor(name) {
        this.name = name;
        this.legs = 4;
        this.cold_blooded = false;
    }
    get Aname() {
        return this.name;
    }
}
 
var sheep = new Animal("shaun");
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

// Soal 1 Animal Class
console.log(" ");
console.log("-------------- Soal 1 Animal Class - Release 1 -----------------");
console.log("---------------menggunakan metoda inheritance-------------------");
// Buatlah class Frog dan class Ape yang merupakan inheritance dari class Animal.
// Perhatikan bahwa Ape (Kera) merupakan hewan berkaki 2, 
// hingga dia tidak menurunkan sifat jumlah kaki 4 dari class Animal. 
// class Ape memiliki function yell() yang menampilkan “Auooo” dan 
// class Frog memiliki function jump() yang akan menampilkan “hop hop”.

class Animal2 {
    constructor(name2) {
        this.name2 = name2;
    }
    present() {
        return "Binatang ini " + this.name2;
    }
}

class Ape extends Animal2 {
    constructor(name2) {
        super(name2);
        this.legs2 = 2;
    }
    yell() {
        return this.present() + ' teriak AUOOOO AUOOOOO dg menggantungkan ' + this.legs2 + ' nya';
    }
}
class Frog extends Animal2 {
    constructor(name2) {
        super(name2);
        this.legs2 = 4;
    }
    jump() {
        return this.present() + ' melompat HOP HOP dg ke ' + this.legs2 + ' nya';
    }
}

var sungokong = new Ape("kera sakti")
console.log(sungokong.yell()) // "Auooo"
 
var kodok = new Frog("buduk")
console.log(kodok.jump()); // "hop hop" 


// Soal 2 Function to Class
console.log(" ");
console.log("-------------- Soal 2 Function to Class -----------------");
console.log("--------mengubah function Clock menjadi class------------");
// Terdapat sebuah class dengan nama Clock yang ditulis seperti penulisan pada function,
// ubahlah fungsi tersebut menjadi class dan pastikan fungsi tersebut tetap berjalan
// dengan baik. Jalankan fungsi di terminal/console Anda untuk melihat hasilnya.
// (tekan tombol Ctrl + C pada terminal untuk menghentikan method clock.start())
// *****************************************
// function Clock({ template }) {
//     var timer;
//     function render() {
//       var date = new Date();
//       var hours = date.getHours();
//       if (hours < 10) hours = '0' + hours;
//       var mins = date.getMinutes();
//       if (mins < 10) mins = '0' + mins;
//       var secs = date.getSeconds();
//       if (secs < 10) secs = '0' + secs;
//       var output = template
//         .replace('h', hours)
//         .replace('m', mins)
//         .replace('s', secs);
//       console.log(output);
//     }
//     this.stop = function() {
//       clearInterval(timer);
//     };
//     this.start = function() {
//       render();
//       timer = setInterval(render, 1000);
//     };
//   }
//   var clock = new Clock({template: 'h:m:s'});
//   clock.start(); 
// ********************************************

class Clock {
    constructor({template}) {
        this.template = template;
    }
    render() {
        var date = new Date();
        var hours = date.getHours();
        if(hours < 10) hours = "0" + hours;
        var mins = date.getMinutes();
        if(mins < 10) mins = "0" + mins;
        var secs = date.getSeconds();
        if(secs < 10) secs = "0" + secs;

        var output = this.template
            .replace("h", hours)
            .replace("m", mins)
            .replace("s", secs);
        console.log(output);
    }
    stop() {
        clearInterval(this.timer);
    }
    start() {
        this.render();
        this.timer = setInterval(this.render.bind(this), 1000);
    }
}

var clock = new Clock({template: 'h:m:s'});
console.log(clock.start());  