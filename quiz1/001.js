// Soal A Membandingkan dua buah angka
console.log("Soal A membandingkan dua buah angka");
console.log("------------------------------------");

var number1 = [];
var number2 = [];


function bandingkan (number1, number2) {
    if ( number1 > 0 || number2 >0) { 
        if(number1<number2){
            var result = number2;
        }
        if(number1 == number2) {
            var result = -1;
        }
        return result;
    }
    if (number1 < 0 || number2 > 0) {
        var result = -1;
        return result;
    }
    if (number1 < 0 || number2 < 0) {
        var result = -1
        return result;
    }
    if (number1 == " " && number2 == " ") {
        var result = -1
        return result;
    }
    
}

// TEST CASES Bandingkan Angka
console.log(bandingkan(10, 15)); // 15
console.log(bandingkan(12, 12)); // -1
console.log(bandingkan(-1, 10)); // -1 
console.log(bandingkan(112, 121));// 121
console.log(bandingkan(1)); // 1
console.log(bandingkan()); // -1
console.log(bandingkan("15", "18")) // 18



// Soal B Balik String
console.log("SOal B Balik String ");
console.log("--------------------------");
function balikString(word) {
    var hasil = " ";
    for(var j = word.length - 1; j>=0; j--) {
        hasil += word[j];
    }
    return hasil;
};

// TEST CASES BalikString
console.log(balikString("abcde")) // edcba
console.log(balikString("rusak")) // kasur
console.log(balikString("racecar")) // racecar
console.log(balikString("haji")) // ijah


// Soal C Palindrome
console.log("SOal C Palindrome ");
console.log("--------------------------");

function palindrome(str) {
    let i = 0;
    let j = str.length - 1;
    while(i < j) {
        if(str[i] == str[j]) {
            i++; 
            j--;
        }
        else {
            return false;
        }
    }
    return true;
}

// TEST CASES Palindrome
console.log(palindrome("kasur rusak")) // true
console.log(palindrome("haji ijah")) // true
console.log(palindrome("nabasan")) // false
console.log(palindrome("nababan")) // true
console.log(palindrome("jakarta")) // false

