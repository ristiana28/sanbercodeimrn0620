// Soal A Descending Ten 
console.log(" Soal A Descending Ten");
console.log("--------------------------");

function DescendingTen(num) {
    var hasil2 = [];
    for(var i = num; i >= num-9; i--) {
        hasil2.push(i);
    }
    return hasil2;

    if(num == " ") {
        return -1;
    }
};

// TEST CASES Descending Ten
console.log(DescendingTen(100)) // 100 99 98 97 96 95 94 93 92 91
console.log(DescendingTen(10)) // 10 9 8 7 6 5 4 3 2 1
console.log(DescendingTen()) // -1


// Soal B Ascending Ten 
console.log(" Soal B Ascending Ten");
console.log("--------------------------");

function AscendingTen(num2) {
    var hasil3 = [];
    for(var i = num2; i <= num2+9; i++) {
        hasil3.push(i);
    }
    return hasil3;

   if(num == " ") {
        return -1;
    }
};

// TEST CASES Ascending Ten
console.log(AscendingTen(11)) // 11 12 13 14 15 16 17 18 19 20
console.log(AscendingTen(21)) // 21 22 23 24 25 26 27 28 29 30
console.log(AscendingTen()) // -1


// Soal C Conditional ascending dan descending
console.log(" Soal C Conditional Ascending Ten dan Descending");
console.log("--------------------------");

function Conditional(aw, ba) {
    var num = [ ];
    var num2 = [ ]
    if(aw % 2 == 0) {
        var hasil2 = [];
        for(var i = num; i >= num-9; i--) {
            hasil2.push(i);
    }
        return hasil2;
        
    } else if(aw % 2 == 1) {
        var hasil3 = [];
        for(var i = num2; i <= num2+9; i++) {
            hasil3.push(i);
        }
        return hasil3;
    }
    
}

// TEST CASES Conditional Ascending Descending
console.log(Conditional(20, 8)) // 20 19 18 17 16 15 14 13 12 11
console.log(Conditional(81, 1)) // 81 82 83 84 85 86 87 88 89 90
console.log(Conditional(31)) // -1
console.log(Conditional()) // -1

