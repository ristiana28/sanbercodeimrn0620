// Soal 1 Looping While
var maju = 2;
var mundur = 20;

function soal1() {
    console.log('Soal 1 Looping While ================');
    console.log('LOOPING PERTAMA');
    while(maju < 21) {
        console.log(maju + ' I love coding');
        maju = maju + 2;
    }

    console.log('LOOPING KEDUA');
    while(mundur > 0) {
        console.log(mundur + ' I will become a mobile developer')
        mundur = mundur - 2;
    }
}

// Output:
// LOOPING PERTAMA
// 2 I love coding
// 4 I love coding
// 6 I love coding
// 8 I love coding
// 10 I love coding
// 12 I love coding
// 14 I love coding
// 16 I love coding
// 18 I love coding
// 20 I love coding
// LOOPING KEDUA
// 20 I will become a mobile developer
// 18 I will become a mobile developer
// 16 I will become a mobile developer
// 14 I will become a mobile developer
// 12 I will become a mobile developer
// 10 I will become a mobile developer
// 8 I will become a mobile developer
// 6 I will become a mobile developer
// 4 I will become a mobile developer
// 2 I will become a mobile developer

// Soal 2 Looping For

function soal2() {
     console.log('Soal 2 Looping For  ================');
    for(var angka = 1; angka < 21; angka++) {
        if (angka % 2 == 1 && angka % 3 == 0) {
            console.log(angka + ' I Love Coding');
        } else if(angka % 2 == 1) {
            console.log(angka + ' Santai');
        } else if(angka % 2 == 0) {
            console.log(angka + ' Berkualitas');
        }
    }
}

// Output :
// 1 Santai
// 2 Berkualitas
// 3 I Love Coding
// 4 Berkualitas
// 5 Santai
// 6 Berkualitas
// 7 Santai
// 8 Berkualitas
// 9 I Love Coding
// 10 Berkualitas
// 11 Santai
// 12 Berkualitas
// 13 Santai
// 14 Berkualitas
// 15 I Love Coding
// 16 Berkualitas
// 17 Santai
// 18 Berkualitas
// 19 Santai
// 20 Berkualitas


// Soal 3 Membuat Persegi Panjang
function soal3() {
    console.log('Soal 3 Persegi Panjang  ==============')
    var panjang = 1
    var pp = ""
    var lebar = 1
    for(panjang; panjang < 5; panjang +=1) {
        for(lebar; lebar < 9; lebar += 1) {
            pp = pp + "#"
        }
        console.log(pp);
    }
}

// Output :
// ########
// ########
// ########
// ########


// Soal 4 Membuat Tangga
function soal4() {
    console.log('Soal 4 Tangga ==============')
    var tinggi = 1
    var ss = ""
    var alas = 1
    for(tinggi; tinggi < 7; tinggi += 1) {
        for(alas; alas < 7; alas += 1) {
            ss = ss + "#"
            if(tinggi == alas) {
                break;
            }
        }
        console.log(ss);
    }
}

// Output :
// # 
// ##
// ###
// ####
// #####
// ######
// #######


// Soal 5 Membuat Papan Catur
function soal5() {
    console.log('Soal 5 Papan Catur ==============')
    
    var hitam = "#";
    var putih = " ";
    var hasil = " ";

    for(var i=0; i < 8; i += 1) {
        for(var j=0; j < 8; j += 1) {
            if(i % 2 == 1) {
                if(j % 2 == 0) {
                    hasil = hasil + hitam;
                } else {
                    hasil = hasil + putih;
                }
            } else {
                if(j % 2 == 0) {
                    hasil = hasil + putih;
                } else {
                    hasil = hasil + hitam;
                }
            }
        }
        hasil = hasil + putih;        
        console.log(hasil);
        hasil = " "
    }
}

// Output :
//  # # # # 
// # # # #  
//  # # # # 
// # # # #  
//  # # # # 
// # # # #  
//  # # # # 
// # # # #  



soal1();
soal2();
soal3();
soal4();
soal5();


