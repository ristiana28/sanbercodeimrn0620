// soal 1 variables
var word = 'JavaScript'; 
var second = 'is'; 
var third = 'awesome'; 
var fourth = 'and'; 
var fifth = 'I'; 
var sixth = 'love'; 
var seventh = 'it!';

function soal1(){
    console.log('Soal 1', '======================')
    console.log(word + ' ' + second + ' ' + third + ' ' + fourth + ' ' + fifth + ' ' + sixth + ' ' + seventh)
}
// Output : JavaScript is awasome and I love it!


// soal 2 Akses karakter dalam string
var sentence = "I am going to be React Native Developer"; 

var firstWord = sentence[0] ; 
var secondWord = sentence[2] + sentence[3]  ; 
var thirdWord = sentence.slice(5, 10); 
var fourthWord = sentence[11] + sentence[12] ;
var fifthWord = sentence[14] + sentence[15] ;
var sixthWord = sentence.slice(17, 22);
var seventhWord = sentence.slice(23, 29) ;
var eighthWord = sentence.slice(30,39);

function soal2() {
    console.log('Soal 2', '======================')
    console.log('First Word: ' + firstWord); 
    console.log('Second Word: ' + secondWord); 
    console.log('Third Word: ' + thirdWord); 
    console.log('Fourth Word: ' + fourthWord); 
    console.log('Fifth Word: ' + fifthWord); 
    console.log('Sixth Word: ' + sixthWord); 
    console.log('Seventh Word: ' + seventhWord); 
    console.log('Eighth Word: ' + eighthWord);
}

// Output :
// First word: I 
// Second word: am 
// Third word: going 
// Fourth word: to 
// Fifth word: be 
// Sixth word: React 
// Seventh word: Native 
// Eighth word: Developer


// Soal 3 Substring
var sentence2 = 'wow JavaScript is so cool'; 

var firstWord2 = sentence2.substring(0, 3); 
var secondWord2 = sentence2.substring(4, 14); 
var thirdWord2 = sentence2.substring(15, 17); 
var fourthWord2 = sentence2.substring(18, 20); 
var fifthWord2 = sentence2.substring(21, 25); 

function soal3() {
    console.log('Soal 3', '======================')
    console.log('First Word: ' + firstWord2); 
    console.log('Second Word: ' + secondWord2); 
    console.log('Third Word: ' + thirdWord2); 
    console.log('Fourth Word: ' + fourthWord2); 
    console.log('Fifth Word: ' + fifthWord2);
}

// Output
// First Word: wow 
// Second Word: JavaScript 
// Third Word: is 
// Fourth Word: so 
// Fifth Word: cool 


// Soal 4 Mengurai Kalimat dan Menentukan Panjang String
var sentence3 = 'wow JavaScript is so cool'; 

var firstWord3 = sentence3.substring(0, 3); 
var secondWord3 = sentence3.substring(4, 14); 
var thirdWord3 = sentence3.substring(15, 17); 
var fourthWord3 = sentence3.substring(18, 20); 
var fifthWord3 = sentence3.substring(21, 25); 

var firstWordLength = firstWord3.length 
var secondWordLength = secondWord3.length 
var thirdWordLength = thirdWord3.length 
var fourthWordLength = fourthWord3.length 
var fifthWordLength = fifthWord3.length 

function soal4() {
    console.log('Soal 4', '======================')
    console.log('First Word: ' + firstWord3 + ', with length: ' + firstWordLength); 
    console.log('Second Word: ' + secondWord3 + ', with length: ' + secondWordLength); 
    console.log('Third Word: ' + thirdWord3 + ', with length: ' + thirdWordLength); 
    console.log('Fourth Word: ' + fourthWord3 + ', with length: ' + fourthWordLength); 
    console.log('Fifth Word: ' + fifthWord3 + ', with length: ' + fifthWordLength); 
}

// Output :
// First Word: wow, with length: 3 
// Second Word: JavaScript, with length: 10 
// Third Word: is, with length: 2 
// Fourth Word: so, with length: 2 
// Fifth Word: cool, with length: 4

soal1();
soal2();
soal3();
soal4();

